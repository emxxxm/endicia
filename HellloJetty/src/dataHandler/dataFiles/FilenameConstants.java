package dataHandler.dataFiles;

public class FilenameConstants {

	public static String APOFPODPO = "ATF_APOFPODPO.txt";
	public static String ADDRESS_CLOSE = "ATF_ADDRESS_CLOSE.txt";
	public static String PME_DEST = "ATF_PME_DEST_SCHEDULE.txt";
	public static String PME_DISP = "ATF_PME_DISP_SCHEDULE.txt";
	public static String REF_VAL = "ATF_REF_VALUE.txt";
	public static String COT_ALL = "ATF_COT_ALL.txt";
	public static String SERVICE_STANDARD_ALL = "ATF_NON_PME_SVC_STD_ALL.txt";
	public static String ORIGIN_SCHEDULE_ALL = "ATF_PME_ORIGIN_SCHEDULE_ALL.txt";
	
}
