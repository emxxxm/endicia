package MainPackage;

public class InvalidQueryFormatException extends Exception {
	InvalidQueryFormatException(String m) {
		super(m);
	}
}
